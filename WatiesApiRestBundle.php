<?php

namespace Waties\ApiRestBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class WatiesApiRestBundle
 * @package Waties\ApiRestBundle
 */
class WatiesApiRestBundle extends Bundle
{
    // Bundle driver list.
    const DRIVER_DOCTRINE_ORM         = 'doctrine/orm';
    const DRIVER_DOCTRINE_MONGODB_ODM = 'doctrine/mongodb-odm';
}