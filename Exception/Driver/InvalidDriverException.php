<?php

namespace Waties\ApiRestBundle\Exception\Driver;

/**
 * Class InvalidDriverException
 * @package Waties\ApiRestBundle\Exception\Driver
 */
class InvalidDriverException extends \Exception
{
    /**
     * @param string $driver
     * @param int $className
     */
    public function __construct($driver, $className)
    {
        parent::__construct(sprintf(
            'Driver "%s" is not supported by %s.',
            $driver,
            $className
        ));
    }
}