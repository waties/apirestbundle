<?php

namespace Waties\ApiRestBundle\Exception\Driver;

/**
 * Class UnknownDriverException
 * @package Waties\ApiRestBundle\Exception\Driver
 */
class UnknownDriverException extends \Exception
{
    /**
     * @param string $driver
     */
    public function __construct($driver)
    {
        parent::__construct(sprintf(
            'Unknown driver "%s"',
            $driver
        ));
    }
}