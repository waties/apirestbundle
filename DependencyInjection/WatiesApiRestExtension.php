<?php

namespace Waties\ApiRestBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Waties\ApiRestBundle\DependencyInjection\Driver\DatabaseDriverFactory;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class WatiesApiRestExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('config_waties_api_rest', $config);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $classes = isset($config['resources']) ? $config['resources'] : array();

        $this->createResourceServices($classes, $container);
    }

    /**
     * @param array            $configs
     * @param ContainerBuilder $container
     */
    private function createResourceServices(array $configs, ContainerBuilder $container)
    {
        foreach ($configs as $name => $config) {
            list($prefix, $resourceName) = explode('.', $name);
            $manager = isset($config['object_manager']) ? $config['object_manager'] : 'default';

            DatabaseDriverFactory::get(
                $container,
                $prefix,
                $resourceName,
                $manager,
                $config['driver']
            )->load($config['classes']);
        }
    }
}
