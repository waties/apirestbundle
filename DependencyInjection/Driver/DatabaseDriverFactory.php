<?php

namespace Waties\ApiRestBundle\DependencyInjection\Driver;

use Waties\ApiRestBundle\WatiesApiRestBundle;
use Waties\ApiRestBundle\Exception\Driver\UnknownDriverException;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class DatabaseDriverFactory
 * @package Waties\ApiRestBundle\DependencyInjection\Driver
 */
class DatabaseDriverFactory
{
    public static function get(
        ContainerBuilder $container,
        $prefix,
        $resourceName,
        $managerName,
        $type = WatiesApiRestBundle::DRIVER_DOCTRINE_ORM
    )
    {
        switch ($type) {
            case WatiesApiRestBundle::DRIVER_DOCTRINE_ORM:
                return new DoctrineORMDriver($container, $prefix, $resourceName, $managerName);
            case WatiesApiRestBundle::DRIVER_DOCTRINE_MONGODB_ODM:
                return new DoctrineODMDriver($container, $prefix, $resourceName, $managerName);
            default:
                throw new UnknownDriverException($type);
        }
    }
}
