<?php

namespace Waties\ApiRestBundle\DependencyInjection\Driver;

/**
 * Interface DatabaseDriverInterface
 * @package Waties\ApiRestBundle\DependencyInjection\Driver
 */
interface DatabaseDriverInterface
{
    public function load(array $classes);

    public function getSupportedDriver();
}
