<?php

namespace Waties\ApiRestBundle\DependencyInjection\Driver;

use Waties\ApiRestBundle\WatiesApiRestBundle;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Parameter;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class DoctrineODMDriver
 * @package Waties\ApiRestBundle\DependencyInjection\Driver
 */
class DoctrineODMDriver extends AbstractDatabaseDriver
{
    /**
     * {@inheritdoc}
     */
    public function getSupportedDriver()
    {
        return WatiesApiRestBundle::DRIVER_DOCTRINE_MONGODB_ODM;
    }

    /**
     * {@inheritdoc}
     */
    protected function getRepositoryDefinition(array $classes)
    {
        $repositoryClass = new Parameter('waties.mongodb_odm.repository.class');

        if (isset($classes['repository'])) {
            $repositoryClass = $classes['repository'];
        }

        $unitOfWorkDefinition = new Definition('Doctrine\\ODM\\MongoDB\\UnitOfWork');
        $unitOfWorkDefinition
            ->setFactoryService($this->getManagerServiceKey())
            ->setFactoryMethod('getUnitOfWork')
            ->setPublic(false);

        $definition = new Definition($repositoryClass);
        $definition->setArguments(array(
            new Reference($this->getContainerKey('manager')),
            $unitOfWorkDefinition,
            $this->getClassMetadataDefinition($classes['model']),
        ));

        return $definition;
    }

    /**
     * {@inheritdoc}
     */
    protected function getManagerServiceKey()
    {
        return sprintf('doctrine_mongodb.odm.%s_document_manager', $this->managerName);
    }

    /**
     * {@inheritdoc}
     */
    protected function getClassMetadataClassname()
    {
        return 'Doctrine\\ODM\\MongoDB\\Mapping\\ClassMetadata';
    }
}
