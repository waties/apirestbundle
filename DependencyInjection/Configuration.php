<?php

namespace Waties\ApiRestBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Waties\ApiRestBundle\WatiesApiRestBundle;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('waties_api_rest');

        $rootNode
            ->children()
                ->arrayNode('resources')
                    ->useAttributeAsKey('name')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('driver')->defaultValue(WatiesApiRestBundle::DRIVER_DOCTRINE_MONGODB_ODM)->end()
                            ->scalarNode('object_manager')->defaultValue('default')->end()
                            ->arrayNode('classes')
                                ->children()
                                    ->scalarNode('model')->isRequired()->cannotBeEmpty()->end()
                                    ->scalarNode('controller')->defaultValue('Waties\ApiRestBundle\Controller\ResourceController')->end()
                                    ->scalarNode('repository')->end()
                                ->end()
                            ->end()
                            ->variableNode('customViewList')->end()
                        ->end()
                    ->end()
                ->end()
                ->scalarNode('basePath')->defaultValue("/api")->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
