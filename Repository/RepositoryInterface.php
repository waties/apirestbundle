<?php

namespace Waties\ApiRestBundle\Repository;

use Doctrine\Common\Persistence\ObjectRepository;

/**
 * Model repository interface.
 *
 * Interface RepositoryInterface
 * @package Waties\ApiRestBundle\Repository
 */
interface RepositoryInterface extends ObjectRepository
{
    /**
     * Create a new resource
     *
     * @return mixed
     */
    public function createNew();

    /**
     * Get paginated collection
     *
     * @param array $criteria
     * @param array $sorting
     *
     * @return mixed
     */
    public function createPaginator(array $criteria = array(), array $sorting = array());

    /**
     * @param array $criteria
     * @param array $sorting
     *
     * @return mixed
     */
    public function getQueryBuilderSearch(array $criteria = array(), array $sorting = array());
    
}