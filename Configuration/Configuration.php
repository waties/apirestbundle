<?php

namespace Waties\ApiRestBundle\Configuration;

use Doctrine\Common\Inflector\Inflector;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;

/**
 * Resource controller configuration.
 *
 * Class Configuration
 * @package Waties\ApiRestBundle\Controller
 */
class Configuration
{
    /**
     * @var string
     */
    protected $bundlePrefix;

    /**
     * @var string
     */
    protected $resourceName;

    /**
     * @var Parameters
     */
    protected $parameters;

    /**
     * Current request.
     *
     * @var Request
     */
    protected $request;

    /**
     * @param $bundlePrefix
     * @param $resourceName
     */
    public function __construct($bundlePrefix, $resourceName)
    {
        $this->bundlePrefix = $bundlePrefix;
        $this->resourceName = $resourceName;
        $this->parameters = new ParameterBag();
    }

    /**
     * @return ParameterBag
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    public function setParameters(ParameterBag $parameters)
    {
        $this->parameters = $parameters;

        return $this;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * @return string
     */
    public function getBundlePrefix()
    {
        return $this->bundlePrefix;
    }

    /**
     * @return string
     */
    public function getResourceName()
    {
        return $this->resourceName;
    }

    /**
     * @return string
     */
    public function getPluralResourceName()
    {
        return Inflector::pluralize($this->resourceName);
    }

    /**
     * @param $service
     * @return string
     */
    public function getServiceName($service)
    {
        return sprintf('%s.%s.%s', $this->bundlePrefix, $service, $this->resourceName);
    }

    /**
     * @return mixed
     */
    public function getFormType()
    {
        return $this->parameters->get('form', sprintf('%s_%s', $this->bundlePrefix, $this->resourceName));
    }

    /**
     * @return array
     */
    public function getSorting()
    {
        $sort = $this->request->query->get('sort') ? explode(',', $this->request->query->get('sort')) : ['id'];
        $sortByDesc = $this->request->query->get('desc') ? explode(',', $this->request->query->get('desc')) : ['id'];
        $sorting = [];
        foreach ($sort as $property) {
            if (in_array($property, $sortByDesc)) {
                $sorting[$property] = 'desc';
            } else {
                $sorting[$property] = 'asc';
            }
        }

        return $sorting;
    }

    /**
     * @param $fieldsOfResource
     * @return array
     */
    public function getCriteria($fieldsOfResource)
    {
        $criteria = [];

        foreach ($this->request->query->all() as $name => $value) {
            if (in_array($name, array_keys($fieldsOfResource))) {
                $criteria[$name] = explode(',', $value);
            }
        }

        return $criteria;
    }

    /**
     * @param $fieldsOfResource
     * @return array
     */
    public function getFields($fieldsOfResource)
    {
        $fields = $this->request->get('fields') ? explode(',', $this->request->get('fields')) : [];
        foreach ($fields as $key => $name) {
            if (!in_array($name, array_keys($fieldsOfResource))) {
                unset($fields[$key]);
            }
        }

        return $fields;
    }

    /**
     * @param $name
     * @return string
     */
    public function getRouteName($name)
    {
        return sprintf('%s_%s_%s', $this->bundlePrefix, $this->resourceName, $name);
    }

    /**
     * @param $parameter
     * @param array $defaults
     * @return array
     */
    public function getRequestParameter($parameter, $defaults = array())
    {
        return array_replace_recursive(
            $defaults,
            $this->request->get($parameter, array())
        );
    }

    /**
     * @param $default
     * @return mixed
     */
    public function getRepositoryMethod($default)
    {
        $repository = $this->parameters->get('repository', array('method' => $default));

        return is_array($repository) ? $repository['method'] : $repository;
    }

    /**
     * @param array $default
     * @return array
     */
    public function getRepositoryArguments(array $default = array())
    {
        $repository = $this->parameters->get('repository', array());

        return isset($repository['arguments']) ? $repository['arguments'] : $default;
    }

    /**
     * @param $default
     * @return mixed
     */
    public function getFactoryMethod($default)
    {
        $factory = $this->parameters->get('factory', array('method' => $default));

        return is_array($factory) ? $factory['method'] : $factory;
    }

    /**
     * @param array $default
     * @return array
     */
    public function getFactoryArguments(array $default = array())
    {
        $factory = $this->parameters->get('factory', array());

        return isset($factory['arguments']) ? $factory['arguments'] : $default;
    }
}
