<?php

namespace Waties\ApiRestBundle\Configuration;

/**
 * Resource controller configuration factory.
 *
 * Class ConfigurationFactory
 * @package Waties\ApiRestBundle\Controller
 */
class ConfigurationFactory
{
    /**
     * @var string
     */
    protected $configurationClass;
    /**
     * Constructor.
     *
     * @param string           $configurationClass
     */
    public function __construct($configurationClass)
    {
        $this->configurationClass = $configurationClass;
    }
    /**
     * Create configuration for given parameters.
     *
     * @param string $bundlePrefix
     * @param string $resourceName
     *
     * @return Configuration
     */
    public function createConfiguration($bundlePrefix, $resourceName)
    {
        return new $this->configurationClass(
            $bundlePrefix,
            $resourceName
        );
    }
}