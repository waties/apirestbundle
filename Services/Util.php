<?php

namespace Waties\ApiRestBundle\Services;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Yaml\Yaml;

class Util
{

    /**
     * @var string
     */
    private $rootDir;

    /**
     * @param $rootDir
     */
    public function __construct($rootDir)
    {
        $this->rootDir = $rootDir;
    }

    /**
     * @return array
     */
    public function getRoutes()
    {
        $locator = new FileLocator($this->rootDir . '/config/');
        $collection = Yaml::parse($locator->locate('routing.yml'));
        $routes = [];
        foreach ($collection as $route) {
            if (isset($route['type']) && $route['type'] === 'waties.api_rest') {
                $key = $route['resource'];
                $routes[$key] = [
                    "actions" => isset($route["defaults"]) && isset($route["defaults"]["actions"]) ? $route["defaults"]["actions"] : [
                        'index',
                        'count',
                        'export',
                        'show',
                        'create',
                        'update',
                        'partial_update',
                        'delete'
                    ]
                ];
                if (isset($route["prefix"])) {
                    $routes[$key]["prefix"] = $route["prefix"];
                }
            }
        }
        return $routes;
    }

    /**
     * @param $resourceClassName
     * @param ObjectManager $objectManager
     * @return array
     */
    public function getFieldsOfResource($resourceClassName, ObjectManager $objectManager)
    {
        $metadata = $objectManager->getClassMetadata($resourceClassName);
        $reflectionClass = new \ReflectionClass($resourceClassName);

        $fields = [];
        $fieldMappings = $metadata->fieldMappings;

        foreach ($fieldMappings as $field) {
            $name = $field['fieldName'];
            if ($name !== 'id' && !in_array($field['type'], ['many', 'one'])) {
                $fields[$name] = ['type' => $field['type']];
            }
        }

        foreach ($metadata->getAssociationNames() as $associationName) {
            $className = $metadata->getAssociationTargetClass($associationName);
            if ($metadata->isCollectionValuedAssociation($associationName)) {
                $type = 'many';
            } else {
                $type = 'one';
            }
            $fields[$associationName] = [
                "type" => $type,
                "resourceClass" => $className
            ];
        }

        return $fields;
    }

    /**
     * @param $resourceClassName
     * @param ObjectManager $objectManager
     * @param $fields
     * @return array
     */
    public function validateFields($resourceClassName, ObjectManager $objectManager, $fields)
    {
        $fieldsOfResource = $this->getFieldsOfResource($resourceClassName, $objectManager);
        if ($fieldsOfResource)
            $fields = array_diff(array_keys($fieldsOfResource), $fields);

        return $fields;
    }

}