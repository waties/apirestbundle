<?php

namespace Waties\ApiRestBundle\Services;

use JMS\Serializer\Serializer;

class Export
{
    private $manager;
    private $results;
    private $fields;

    public function __construct(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param $manager
     * @param $fields
     * @param $results
     * @param $format
     * @throws \Exception
     */
    public function writesFileToFormat($manager, $fields, $results, $format)
    {
        $this->manager = $manager;
        $this->results = $results;
        $this->fields = $fields;

        switch ($format) {
            case 'xlsx':
                $phpExcel = new \PHPExcel();
                $writer = new \PHPExcel_Writer_Excel2007($phpExcel);
                $writer->setPreCalculateFormulas(false);

                $this->writesFileToExcel($phpExcel, $writer);
                break;
            case 'xls':
                $phpExcel = new \PHPExcel();
                $writer = new \PHPExcel_Writer_Excel5($phpExcel);
                $this->writesFileToExcel($phpExcel, $writer);
                break;
            case 'csv':
                $this->writesFileToCsv();
                break;
            default:
                throw new \Exception("\"$format\" format is not supported !");
                break;
        }
    }

    /**
     *
     * Writes the file to XLS/XLSX
     *
     * @param $phpExcel
     * @param $writer
     */
    private function writesFileToExcel($phpExcel, $writer)
    {
        $activeSheet = $phpExcel->getActiveSheet();
        $letter = 'A';

        foreach ($this->fields as $field) {
            $activeSheet->SetCellValue($letter++ . '1', $field);
        }

        $line = 1;
        foreach ($this->results as $resource) {
            $column = 0;
            $line++;
            $resource = current($resource);
            $jsonData = $this->serializer->serialize($resource, 'json');
            $data = json_decode($jsonData, true);

            foreach ($data as $key => $value) {
                if (in_array($key, $this->fields)) {
                    $activeSheet->setCellValueByColumnAndRow($column++, $line, $this->formatValue($value));
                }
            }
        }

        $writer->save('php://output');
    }

    /**
     * @param $value
     * @return mixed
     */
    private function formatValue($value)
    {
        if (is_bool($value)) {
            if ($value == true) {
                return 'oui';
            } else {
                return 'non';
            }
        } elseif (is_scalar($value)) {
            return $value;
        }
    }

    /**
     * Writes the file to CSV
     */
    private function writesFileToCsv()
    {
        $handle = fopen('php://output', 'r+');

        fputcsv($handle, $this->fields, ';');

        foreach ($this->results as $resource) {
            $resource = current($resource);
            $jsonData = $this->serializer->serialize($resource, 'json');
            $data = json_decode($jsonData, true);
            $dataFormated = [];
            foreach ($data as $key => $value) {
                if (in_array($key, $this->fields)) {
                    $dataFormated[$key] = $this->formatValue($value);
                }
            }
            fputcsv($handle, $dataFormated, ';');
            $this->manager->detach($resource);
        }

        fclose($handle);
    }
}