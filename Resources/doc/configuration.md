2. Configuring the API
==================

2.1. Configuration
-------------------------------

To begin with, configure FosUserBundle in your ``app/config.yml`` (or in an imported configuration file) :

```yaml
fos_rest:
    param_fetcher_listener: true
    body_listener: true
    routing_loader:
        include_format: false
    format_listener:
        rules:
            - { path: '^/api', priorities: ['json', 'xml'], fallback_format: ~, prefer_extension: false }
    view:
        view_response_listener: force
        formats:
            json: true
            xml: true
    exception:
        enabled: true
        exception_controller: 'FOS\RestBundle\Controller\ExceptionController::showAction'
```

Then for WatiesApiRestBundle, in your ``app/config.yml`` (or in an imported configuration file), you need to define which resources you want to use :

```yaml
waties_api_rest:
	resources:
        app.post:
            driver:    doctrine/mongodb-odm
            classes:
                model: AppBundle\Document\Post
        app.comment:
            driver:    doctrine/mongodb-odm
            classes:
                model: AppBundle\Document\Comment
        app.tag:
            driver:    doctrine/mongodb-odm
            classes:
                model: AppBundle\Document\Tag
```

2.2. Routing
--------------

In your ``app/routing.yml`` (or in an imported routing file), you need to define which routes and actions you want to use :

```yaml
app_post:
    resource: app.post
    type:     waties.api_rest
    prefix:   /api
    defaults:
        actions: [index, count, show, create, update, partial_update, delete]

app_comment:
    resource: app.comment
    type:     waties.api_rest
    prefix:   /api

app_tag:
    resource: app.tag
    type:     waties.api_rest
    prefix:   /api
```