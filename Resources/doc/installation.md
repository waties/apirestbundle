1. Installation
===============

We assume you're familiar with [Composer](http://packagist.org), a dependency manager for PHP.
Use the following command to add the bundle to your ``composer.json`` and download the package.

If you have [Composer installed globally](http://getcomposer.org/doc/00-intro.md#globally).

```
    $ composer require "waties/api-rest-bundle"
```

Otherwise you have to download .phar file.

```
    $ curl -sS https://getcomposer.org/installer | php
    $ php composer.phar require "waties/api-rest-bundle"
```

1.1. Adding required bundles to the kernel
-------------------------------------

You just need to enable proper bundles inside the kernel.

```php
    <?php

    // app/AppKernel.php

    public function registerBundles()
    {
        $bundles = array(
            new FOS\RestBundle\FOSRestBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle($this),
            new Doctrine\Bundle\MongoDBBundle\DoctrineMongoDBBundle(),
            new Waties\ApiRestBundle\WatiesApiRestBundle()
        );
    }
```