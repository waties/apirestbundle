<?php

namespace Waties\ApiRestBundle\Routing;

use Doctrine\Common\Inflector\Inflector;
use FOS\RestBundle\Routing\RestRouteCollection;
use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Waties\ApiRestBundle\Services\Util;

/**
 * Class ApiLoader
 * @package Waties\ApiRestBundle\Routing
 */
class ApiLoader extends Loader
{

    /**
     * @var bool
     */
    private $loaded = false;

    /**
     * @var array
     */
    private $routes = [];

    /**
     * @param Util $util
     * @param $configWatiesApiRest
     */
    public function __construct(Util $util, $configWatiesApiRest)
    {
        $this->routes = $util->getRoutes();
        $this->configWatiesApiRest = $configWatiesApiRest;
    }

    /**
     * @param mixed $resource
     * @param null $type
     * @return RestRouteCollection
     */
    public function load($resource, $type = null)
    {
        if (true === $this->loaded) {
            throw new \RuntimeException('Do not add the "api" loader twice');
        }

        $configRoute = $this->routes[$resource];
        $routes = new RouteCollection();
        list($prefix, $resource) = explode('.', $resource);

        $pluralResource = Inflector::pluralize($resource);
        $rootPath = '/' . $this->configWatiesApiRest['basePath'] . '/' . $pluralResource;

        $requirements = array();
        $options = array('expose' => true);

        // GET count collection request.
        if (in_array('count', $configRoute['actions'])) {
            $routeName = sprintf('%s_%s_count', $prefix, $resource);
            $defaults = array(
                '_controller' => sprintf('%s.controller.%s:countAction', $prefix, $resource),
            );

            $route = new Route($rootPath . '/count', $defaults, $requirements, array(), '', array(), array('GET'));
            $routes->add($routeName, $route);
        }

        // GET export collection request.
        if (in_array('export', $configRoute['actions'])) {
            $routeName = sprintf('%s_%s_export', $prefix, $resource);
            $defaults = array(
                '_controller' => sprintf('%s.controller.%s:exportAction', $prefix, $resource),
            );


            $route = new Route($rootPath . '/export', $defaults, $requirements, $options, '', array(), array('GET'));
            $routes->add($routeName, $route);
        }

        // GET collection request.
        if (in_array('index', $configRoute['actions'])) {
            $routeName = sprintf('%s_%s_index', $prefix, $resource);
            $defaults = array(
                '_controller' => sprintf('%s.controller.%s:indexAction', $prefix, $resource),
            );

            $route = new Route($rootPath, $defaults, $requirements, $options, '', array(), array('GET'));
            $routes->add($routeName, $route);
        }

        // GET request.
        if (in_array('show', $configRoute['actions'])) {
            $routeName = sprintf('%s_%s_show', $prefix, $resource);
            $defaults = array(
                '_controller' => sprintf('%s.controller.%s:showAction', $prefix, $resource),
            );

            $route = new Route($rootPath . '/' . '{id}', $defaults, $requirements, $options, '', array(), array('GET'));
            $routes->add($routeName, $route);
        }

        // POST request.
        if (in_array('create', $configRoute['actions'])) {
            $routeName = sprintf('%s_%s_create', $prefix, $resource);
            $defaults = array(
                '_controller' => sprintf('%s.controller.%s:createAction', $prefix, $resource),
            );

            $route = new Route($rootPath, $defaults, $requirements, $options, '', array(), array('POST'));
            $routes->add($routeName, $route);
        }

        // PUT request.
        if (in_array('update', $configRoute['actions'])) {
            $routeName = sprintf('%s_%s_update', $prefix, $resource);
            $defaults = array(
                '_controller' => sprintf('%s.controller.%s:updateAction', $prefix, $resource),
            );

            $route = new Route($rootPath . '/' . '{id}', $defaults, $requirements, $options, '', array(), array('PUT'));
            $routes->add($routeName, $route);
        }

        // PATCH request.
        if (in_array('partial_update', $configRoute['actions'])) {
            $routeName = sprintf('%s_%s_partial_update', $prefix, $resource);
            $defaults = array(
                '_controller' => sprintf('%s.controller.%s:partialUpdateAction', $prefix, $resource),
            );

            $route = new Route($rootPath . '/' . '{id}', $defaults, $requirements, $options, '', array(), array('PATCH'));
            $routes->add($routeName, $route);
        }

        // DELETE request.
        if (in_array('delete', $configRoute['actions'])) {
            $routeName = sprintf('%s_%s_delete', $prefix, $resource);
            $defaults = array(
                '_controller' => sprintf('%s.controller.%s:deleteAction', $prefix, $resource),
            );

            $route = new Route($rootPath . '/' . '{id}', $defaults, $requirements, $options, '', array(), array('DELETE'));
            $routes->add($routeName, $route);
        }

        return $routes;
    }

    /**
     * @param mixed $resource
     * @param null|string $type
     * @return bool
     */
    public function supports($resource, $type = null)
    {
        return 'waties.api_rest' === $type;
    }
}
