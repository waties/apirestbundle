WatiesApiRestBundle
===================

[![SensioLabsInsight](https://insight.sensiolabs.com/projects/4dffb53e-ddd0-4ffc-ac38-5bc7c5805735/mini.png)](https://insight.sensiolabs.com/projects/4dffb53e-ddd0-4ffc-ac38-5bc7c5805735)

WatiesApiRestBundle is an easy to use and powerful system to create a API REST.

Official documentation
---------------------------


1. [Installation](Resources/doc/installation.md)
	1. [Adding required bundles to the kernel](Resources/doc/installation.md#adding-required-bundles-to-the-kernel)
1. [Configuring the API](Resources/doc/configuration.md#configuring-the-api)
	1. [Configuration](Resources/doc/configuration.md#configuration)
	1. [Routing](Resources/doc/configuration.md#routing)