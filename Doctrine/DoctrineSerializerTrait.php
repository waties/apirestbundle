<?php

namespace Waties\ApiRestBundle\Doctrine;

trait DoctrineSerializerTrait
{
    private $fieldsSelected = array();
    static $_ignoreFields = array();
    private $_oneDimensional = false;
    private $_formatValueForExport = false;

    /**
     * Convert Doctrine\ORM Document to Array
     *
     * @return array
     */
    public function toArray()
    {
        $document = $this->toStdClass();
        return get_object_vars($document);
    }

    /**
     * Convert Doctrine\ORM Document to Array
     *
     * @return string
     */
    public function toJSON()
    {
        $document = $this->toStdClass();
        return json_encode($document);
    }

    /**
     * Set properties to ignore when serializing
     *
     * @example  $this->setIgnoredFields(array('createdDate', 'secretFlag'));
     *
     * @param array $fields
     */
    public function setIgnoredFields(array $fields)
    {
        self::$_ignoreFields = $fields;
    }

    /**
     * @param array $fieldsSelected
     */
    public function setFieldsSelected(array $fieldsSelected)
    {
        $this->fieldsSelected = $fieldsSelected;
    }

    /**
     * @param bool $oneDimensional
     */
    public function setOneDimensional($oneDimensional)
    {
        $this->_oneDimensional = $oneDimensional;

        return $this;
    }

    /**
     * @param bool $oneDimensional
     */
    public function setFormatValueForExport($formatValueForExport)
    {
        $this->_formatValueForExport = $formatValueForExport;

        return $this;
    }

    /**
     * @return array
     */
    private function findGetters()
    {
        $funcs = get_class_methods(get_class($this));
        $getters = array();
        foreach ($funcs as $func) {
            if (strpos($func, 'get') === 0) {
                $getters[] = $func;
            }
        }
        return $getters;
    }

    /**
     * @return array
     */
    private function getPropertiesAndGetter()
    {
        $propertiesAndGetter = [];
        $getters = $this->findGetters();

        $reflectionClass = new \ReflectionClass($this);
        foreach ($reflectionClass->getProperties() as $property) {
            $propertyName = $property->getName();
            $getter = 'get' . ucfirst($propertyName);
            if (in_array($getter, $getters)) {
                $propertiesAndGetter[$propertyName] = $getter;
            }
        }

        return $propertiesAndGetter;
    }
}