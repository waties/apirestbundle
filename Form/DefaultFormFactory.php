<?php

namespace Waties\ApiRestBundle\Form;

use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Symfony\Component\Form\FormFactoryInterface;

/**
 * Generates a form class based on a Doctrine entity.
 *
 * Class DefaultFormFactory
 * @package Waties\ApiRestBundle\Form
 */
class DefaultFormFactory
{
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @param FormFactoryInterface $formFactory
     */
    public function __construct(FormFactoryInterface $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    /**
     * @param $resource
     * @param ObjectManager $objectManager
     * @return \Symfony\Component\Form\Form
     */
    public function create($resource, ObjectManager $objectManager)
    {
        $metadata = $objectManager->getClassMetadata(get_class($resource));

        if (count($metadata->identifier) > 1) {
            throw new \RuntimeException('The default form factory does not support entity classes with multiple primary keys.');
        }

        $builder = $this->formFactory->createNamedBuilder('', 'form', $resource, array('csrf_protection' => false));

        foreach ($this->getFieldsFromMetadata($metadata) as $field => $type) {
            if (!in_array($field, ['id'])) {
                $options = array();

                if (in_array($type, array('date', 'datetime'))) {
                    $options = array('widget' => 'single_text');
                }
                if ('relation' === $type) {
                    $options = array('property' => 'id');
                }

                switch($type) {
                    case 'string':
                        $builder->add($field, 'text', $options);
                        break;
                    default:
                        $builder->add($field, null, $options);
                        break;
                }
            }
        }

        return $builder->getForm();
    }

    /**
     * Returns an array of fields. Fields can be both column fields and
     * association fields.
     *
     * @param ClassMetadata $metadata
     *
     * @return array $fields
     */
    private function getFieldsFromMetadata(ClassMetadata $metadata)
    {
        $fields = (array) $metadata->getFieldNames();

        if ($metadata instanceof ClassMetadataInfo && !$metadata->isIdentifierNatural()) {
            $fields = array_diff($fields, $metadata->identifier);
        }

        $fieldsMapping = array();

        foreach ($fields as $field) {
            $fieldsMapping[$field] = $metadata->getTypeOfField($field);
        }

        foreach ($metadata->associationMappings as $fieldName => $relation) {
            if ($relation['type'] !== ClassMetadataInfo::ONE_TO_MANY) {
                $fieldsMapping[$fieldName] = 'relation';
            }
        }

        return $fieldsMapping;
    }
}
