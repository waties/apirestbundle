<?php

namespace Waties\ApiRestBundle\Controller;

use Doctrine\Common\Util\Inflector;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations\View;
use Waties\ApiRestBundle\Configuration\Configuration;

class FrontController extends FOSRestController
{

    /**
     * @Route("/api/translations")
     * @View()
     */
    public function translationsAction()
    {
        $translator = $this->get('translator');
        $messageCatalogue = $translator->getCatalogue($this->container->getParameter('locale'));

        $translations = [
            'admin' => $messageCatalogue->all('admin')
        ];

        return $translations;
    }

    /**
     * @Route("/api/resource-config/{resource}")
     * @View()
     */
    public function resourceConfigAction($resource)
    {
        $translator = $this->get('translator');
        $formHorizontal = true;

        $configWatiesApiRest = $this->container->getParameter('config_waties_api_rest');
        foreach ($configWatiesApiRest['resources'] as $controller => $data) {
            $dataConfig = $data;
            $controllerSplit = explode('.', $controller);
            $configResource = new Configuration($controllerSplit[0], $controllerSplit[1]);
            $objectManager = $this->container->get($configResource->getServiceName('manager'));
            $model = $data['classes']['model'];
            $modelSplit = explode('\\', $model);

            $resourceName = strtolower(end($modelSplit));
            $pluralResource = Inflector::pluralize($resourceName);
            if ($pluralResource == $resource) {
                break;
            }
        }

        $config = [
            'resourceName' => $resource,
            'pluralResourceLabel' => $translator->trans($pluralResource, [], $pluralResource),
            'resourceLabel' => $translator->trans($resourceName, [], $pluralResource)
        ];

        $pluralResource = Inflector::pluralize($resource);
        $fields = $this->get('waties.api_rest.util')->getFieldsOfResource($model, $objectManager);

        /*foreach($fields as $name => $field) {
            echo $name . ': ' . $name .'<br />';
        }
        die();*/

        if(isset($dataConfig['customViewList'])) {
            $config['customViewList'] = [];
            foreach($dataConfig['customViewList'] as $columnName) {
                $config['customViewList'][$columnName] = $translator->trans('column_thead.' . $columnName, [], $pluralResource);
            }
        }

        foreach($fields as $fieldName => $field) {
            if (!in_array($fieldName, ['id'])) {
                $config['fields'][$fieldName] = [
                    'label' => $translator->trans('fields.' . $fieldName, [], $pluralResource),
                    'type' => $field['type']
                ];
            }
            if (isset($field['resourceClass'])) {
                $resourceClass = $field['resourceClass'];
                $fieldsRelation = $this->get('waties.api_rest.util')->getFieldsOfResource($resourceClass, $objectManager);
                foreach($fieldsRelation as $fieldNameRelation => $fieldRelation) {
                    if (!in_array($fieldNameRelation, ['id'])) {
                        $config['fields'][$fieldName]['fields'][$fieldNameRelation] = [
                            'label' => $translator->trans($fieldNameRelation, [], $fieldName),
                            'type' => $field['type']
                        ];
                    }
                }
            }
            $config['listView'][$fieldName] = $translator->trans('fields.' . $fieldName, [], $pluralResource);
            $config['form'][$fieldName] = [
                'key' => $fieldName,
                'templateOptions' => [
                    'label' => ucfirst($translator->trans('fields.' . $fieldName, [], $pluralResource))
                ]
            ];
            switch($field['type']) {
                case 'id':
                    $config['form'][$fieldName] = array_merge_recursive(
                        $config['form'][$fieldName], [
                            'type' => 'hidden'
                        ]
                    );
                    break;
                case 'boolean':
                    $config['form'][$fieldName] = array_merge_recursive(
                        $config['form'][$fieldName], [
                            'type' => $formHorizontal ? 'horizontalCheckbox' : 'checkbox'
                        ]
                    );
                    break;
                case 'date':
                    $config['form'][$fieldName] = array_merge_recursive(
                        $config['form'][$fieldName], [
                            'type' => $formHorizontal ? 'horizontalDatepicker' : 'datepicker'
                        ]
                    );
                    break;
                case 'many':
                    $options = [];
                    /*$entities = $objectManager->getRepository($field['resourceClass'])->findAll();
                    var_dump($entities);
                    die();
                    foreach($entities as $entity) {
                        $reflectionClass = new \ReflectionClass($entity);
                        if ($reflectionClass->hasMethod('getName')) {
                            array_push($options, [
                                'name' => $entity->getName(),
                                'value' => $entity->getName()
                            ]);
                        }
                    }*/
                    $config['form'][$fieldName] = array_merge_recursive(
                        $config['form'][$fieldName], [
                            'type' => $formHorizontal ? 'horizontalMultiselect' :  'multiselect',
                            'templateOptions' => [
                                'options' => $options
                            ]
                        ]
                    );
                    break;
                case 'one':
                    $config['form'][$fieldName] = array_merge_recursive(
                        $config['form'][$fieldName], [
                            'type' => $formHorizontal ? 'horizontalSelect' : 'select'
                        ]
                    );
                    break;
                default:
                    $config['form'][$fieldName] = array_merge_recursive(
                        $config['form'][$fieldName], [
                            'type' => $formHorizontal ? 'horizontalInput' : 'input',
                            'templateOptions' => [
                                'type' => 'text'
                            ]
                        ]
                    );
                    break;
            }
        }

        return $config;
    }

    /**
     * @Route("/api/menu-config")
     * @View()
     */
    public function menuConfigAction()
    {
        $translator = $this->get('translator');
        $config = [];
        $config['menu'] = [
            'dashboard' => [
                'icon' => 'fa fa-dashboard',
                'name' => $translator->trans('dashboard', [], 'admin')
            ]
        ];

        $configWatiesApiRest = $this->container->getParameter('config_waties_api_rest');
        $resources = [];
        foreach ($configWatiesApiRest['resources'] as $controller => $resource) {
            $model = $resource['classes']['model'];
            $modelSplit = explode('\\', $model);

            $resourceName = strtolower(end($modelSplit));
            $pluralResource = Inflector::pluralize($resourceName);

            $config['menu'][$pluralResource] = [
                'icon' => null,
                'name' => $translator->trans($pluralResource, [], $pluralResource)
            ];
        }

        return $config;
    }

}