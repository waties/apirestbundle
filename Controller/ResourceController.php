<?php

namespace Waties\ApiRestBundle\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use Waties\ApiRestBundle\Repository\RepositoryInterface;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Waties\ApiRestBundle\Form\DefaultFormFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Waties\ApiRestBundle\Configuration\Configuration;
use Waties\ApiRestBundle\Configuration\ResourceResolver;

/**
 * Base resource controller.
 *
 * Class ResourceController
 * @package Waties\ApiRestBundle\Controller
 */
class ResourceController extends FOSRestController
{
    /**
     * @var Configuration
     */
    protected $config;

    /**
     * @var ResourceResolver
     */
    protected $resourceResolver;

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var RepositoryInterface
     */
    protected $repository;

    /**
     * @var string
     */
    protected $resourceClassName;

    /**
     * @param Configuration $config
     */
    public function __construct(Configuration $config)
    {
        $this->config = $config;
    }

    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);

        $this->resourceResolver = new ResourceResolver($this->config);

        if (null !== $container) {
            $this->objectManager = $this->getObjectManager();
            $this->repository = $this->getRepository();
            $this->resourceClassName = $this->repository->getClassName();
        }
    }

    /**
     * @return ObjectManager
     */
    public function getObjectManager()
    {
        return $this->container->get($this->config->getServiceName('manager'));
    }

    /**
     * @return RepositoryInterface
     */
    public function getRepository()
    {
        return $this->container->get($this->config->getServiceName('repository'));
    }

    /**
     * @param Request $request
     * @param integer $id
     * @return View
     */
    public function showAction(Request $request, $id)
    {
        $resource = $this->findByIdOr404($id);

        return $this->view($resource);
    }

    public function searchEnhanced() {

    }

    /**
     * @param Request $request
     * @return View
     */
    public function indexAction(Request $request)
    {
        /* Parameters */
        $perPage = $request->query->getInt('per_page', 25);
        $page = $request->query->getInt('page', 1);

        $this->config->setRequest($request);
        $sorting = $this->config->getSorting();
        $fieldsOfResource = $this->get('waties.api_rest.util')->getFieldsOfResource($this->resourceClassName, $this->objectManager);
        $criteria = $this->config->getCriteria($fieldsOfResource);
        $fields = $this->config->getFields($fieldsOfResource);

        $paginator = $this->repository
            ->createPaginator($criteria, $sorting);

        $paginator
            ->setMaxPerPage($perPage)
            ->setCurrentPage($page);

        $serializer = $this->container->get('jms_serializer');

        /*echo '<pre>';
        $metadataFactory = $serializer->getMetadataFactory();
        $propertyMetadata = $metadataFactory->getMetadataForClass($this->resourceClassName)->propertyMetadata;
        var_dump(array_keys($propertyMetadata));
        var_dump($propertyMetadata);*/
        
        $resources = [];
        foreach ($paginator->getCurrentPageResults() as $resource) {
            $json = $serializer->serialize($resource, 'json');
            array_push($resources, json_decode($json, true));
        }

        if ($paginator->getNbPages() == 1) {
            return $this->view([
                "resources" => $resources
            ], Response::HTTP_OK);
        }

        return $this->view([
            'info' => [
                'nb_results' => $paginator->getNbResults(),
                'nb_pages' => $paginator->getNbPages(),
                'per_page' => $perPage,
                'page' => $page
            ],
            'resources' => $resources
        ], Response::HTTP_PARTIAL_CONTENT);
    }

    /**
     * @param Request $request
     * @return View
     */
    public function countAction(Request $request)
    {
        $this->config->setRequest($request);
        $sorting = $this->config->getSorting();
        $fieldsOfResource = $this->get('waties.api_rest.util')->getFieldsOfResource($this->resourceClassName, $this->objectManager);
        $criteria = $this->config->getCriteria($fieldsOfResource);

        $count = $this->repository
            ->countSearch($criteria, $sorting);

        return $this->view([
            "nb_results" => $count
        ], Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @return StreamedResponse
     */
    public function exportAction(Request $request)
    {
        /* Parameters */
        $format = $request->query->get('format');

        if (empty($format)) {
            throw new \Exception('Required exported file format !');
        }

        $fieldsOfResource = $this->get('waties.api_rest.util')->getFieldsOfResource($this->resourceClassName, $this->objectManager);
        $this->config->setRequest($request);

        /* Parameters */
        $fields = $this->config->getFields($fieldsOfResource);
        $sorting = $this->config->getSorting();
        $criteria = $this->config->getCriteria($fieldsOfResource);

        $queryBuilder = $this->repository
            ->getQueryBuilderSearch($criteria, $sorting);

        $export = $this->get('waties.api_rest.export');

        $objectManager = $this->objectManager;

        $response = new StreamedResponse(function () use ($export, $queryBuilder, $objectManager, $fields, $format) {
            $results = $queryBuilder->getQuery()->iterate();
            $export->writesFileToFormat($objectManager, $fields, $results, $format);
        });

        $response->headers->set('Content-Type', 'application/force-download');
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $this->config->getPluralResourceName() . '.' . $format . '"');

        return $response;
    }

    /**
     * @param Request $request
     * @return View|Response
     */
    public function createAction(Request $request)
    {
        $resource = $this->createNew();
        return $this->processForm($resource, $request);
    }

    /**
     * @param Request $request
     * @param $id
     * @return View|Response
     */
    public function updateAction(Request $request, $id)
    {
        $resource = $this->findByIdOr404($id);

        return $this->processForm($resource, $request);
    }

    /**
     * @param Request $request
     * @param $id
     * @return View|Response
     */
    public function partialUpdateAction(Request $request, $id)
    {
        $resource = $this->findByIdOr404($id);

        return $this->processForm($resource, $request);
    }

    /**
     * @param Request $request
     * @param integer $id
     * @return View
     */
    public function deleteAction(Request $request, $id)
    {
        $resource = $this->findByIdOr404($id);

        $this->objectManager->remove($resource);
        $this->objectManager->flush();

        return View::create(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @return object
     */
    private function createNew()
    {
        return $this->resourceResolver->createResource($this->repository, 'createNew');
    }

    /**
     * @param object|null $resource
     *
     * @return FormInterface
     */
    private function getForm($resource = null)
    {
        $type = $this->config->getFormType();
        $formFactory = $this->container->get('form.factory');

        if (strpos($type, '\\') !== false) { // full class name specified
            $type = new $type();
        } elseif (!$this->get('form.registry')->hasType($type)) { // form alias is not registered

            $defaultFormFactory = new DefaultFormFactory($formFactory);

            return $defaultFormFactory->create($resource, $this->objectManager);
        }

        if ($this->config->isApiRequest()) {
            return $this->container->get('form.factory')->createNamed('', $type, $resource, array('csrf_protection' => false));
        }

        return $this->createForm($type, $resource);
    }

    /**
     * @param $resource
     * @param Request $request
     * @return View|Response
     */
    private function processForm($resource, Request $request)
    {
        $serializer = $this->container->get('jms_serializer');
        $statusCode = $resource->getId() !== false ? Response::HTTP_CREATED : Response::HTTP_NO_CONTENT;

        $form = $this->getForm($resource);

        $data = $request->request->all();

        if ($request->isMethod('PATCH')) {
            $json = $serializer->serialize($resource, 'json');
            $data = array_merge(json_decode($json, true), $data);
            unset($data['id']);
        }

        if ($form->submit($data)->isValid()) {

            $this->objectManager->persist($resource);
            $this->objectManager->flush();

            $json = $serializer->serialize($resource, 'json');
            $data = json_decode($json, true);

            if (Response::HTTP_CREATED === $statusCode) {
                return $this->view($data, $statusCode)
                    ->setHeader('Location',
                        $this->generateUrl(
                            $this->config->getRouteName('show'), array('id' => $resource->getId()),
                            true
                        )
                    );
            }

            return $this->view($data, $statusCode);
        }

        return $this->view($form, Response::HTTP_BAD_REQUEST);
    }

    /**
     * @param string $id
     *
     * @return object
     *
     * @throws NotFoundHttpException
     */
    private function findByIdOr404($id)
    {
        $resource = $this->repository->find($id);
        $resourceClassName = $this->resourceClassName;

        if (!$resource instanceof $resourceClassName) {
            throw new NotFoundHttpException('Resource not found');
        }

        return $resource;
    }
}
